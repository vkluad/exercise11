# Exercise 11
Run `make all && sudo ./test`

## Result:
### Subtask01
```
Hello, world!
Hello, world!
Hello, world!
cat: /dev/abc3: No such device or address
/dev/abc  /dev/abc1  /dev/abc2	/dev/abc3
/sys/module/fixdev
├── coresize
├── holders
├── initsize
├── initstate
├── notes
├── parameters
│   └── major
├── refcnt
├── sections
│   ├── __mcount_loc
│   └── __param
├── srcversion
├── taint
├── uevent
└── version

4 directories, 11 files
[ 8019.920685] =========== module installed 511:0 ==============
[ 8019.921851] === read : 131072
[ 8019.921854] === read return : 14
[ 8019.921863] === read : 131072
[ 8019.921864] === read return : 0
[ 8019.922891] === read : 131072
[ 8019.922893] === read return : 14
[ 8019.922898] === read : 131072
[ 8019.922899] === read return : 0
[ 8019.923895] === read : 131072
[ 8019.923897] === read return : 14
[ 8019.923902] === read : 131072
[ 8019.923903] === read return : 0
[ 8019.930758] =============== module removed ==================
```

### Subtask 02

```
/dev/dyn_0  /dev/dyn_1	/dev/dyn_2
/sys/module/dyndev
├── coresize
├── holders
├── initsize
├── initstate
├── notes
├── parameters
│   └── major
├── refcnt
├── sections
│   ├── __mcount_loc
│   └── __param
├── srcversion
├── taint
├── uevent
└── version

4 directories, 11 files
511 my_dyndev_mod
dyndev 16384 0 - Live 0xffffffffc0aaf000 (OE)
Hello, world!
Hello, world!
Hello, world!
total 0
lrwxrwxrwx 1 root root 0 Sep  9 15:22 dyn_0 -> ../../devices/virtual/dyn_class/dyn_0
lrwxrwxrwx 1 root root 0 Sep  9 15:22 dyn_1 -> ../../devices/virtual/dyn_class/dyn_1
lrwxrwxrwx 1 root root 0 Sep  9 15:22 dyn_2 -> ../../devices/virtual/dyn_class/dyn_2
[ 8019.971178] ======== module installed 511:[0-2] ===========
[ 8019.982352] === read : 131072
[ 8019.982356] === read return : 14
[ 8019.982362] === read : 131072
[ 8019.982363] === read return : 0
[ 8019.982380] === read : 131072
[ 8019.982381] === read return : 14
[ 8019.982384] === read : 131072
[ 8019.982385] === read return : 0
[ 8019.982391] === read : 131072
[ 8019.982391] === read return : 14
[ 8019.982394] === read : 131072
[ 8019.982395] === read return : 0
[ 8019.988313] =============== module removed ==================
```
